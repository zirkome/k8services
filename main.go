package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/heptiolabs/healthcheck"

	"github.com/go-chi/chi"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"

	"gitlab.com/zirkome/k8services/server"
)

var (
	port       = flag.Int("port", 8080, "The port for the HTTP server.")
	healthPort = flag.Int("health-port", 8086, "The port for the HTTP healthcheck server.")
	kubeconfig = flag.String("kubeconfig", os.Getenv("KUBECONFIG"), "The kubeconfig path.")
	inCluster  = flag.Bool("in-cluster", false, "Configure the service to run in-cluster.")
)

func main() {
	flag.Parse()

	logger := log.NewLogfmtLogger(log.NewSyncWriter(os.Stderr))
	logger = level.NewFilter(logger, level.AllowInfo())
	logger = log.With(logger, "ts", log.DefaultTimestampUTC, "caller", log.DefaultCaller)

	var config *rest.Config
	if *inCluster {
		var err error
		// creates the in-cluster config
		config, err = rest.InClusterConfig()
		if err != nil {
			panic(err.Error())
		}
	} else if *kubeconfig != "" {
		var err error
		config, err = clientcmd.BuildConfigFromFlags("", *kubeconfig)
		if err != nil {
			panic(err.Error())
		}
	}

	// creates the clientset
	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		panic(err.Error())
	}

	svc := server.New(log.With(logger, "api", "services"), clientset)

	// healthcheck
	health := healthcheck.NewHandler()
	health.AddReadinessCheck(
		"upstream-k8s-api-http",
		func() error {
			res := clientset.RESTClient().Get().AbsPath("/healthz").Do()
			if res.Error() != nil {
				return res.Error()
			}
			return nil
		})
	health.AddLivenessCheck(
		"upstream-k8s-api-http",
		func() error {
			res := clientset.RESTClient().Get().AbsPath("/healthz").Do()
			if res.Error() != nil {
				return res.Error()
			}
			return nil
		})
	go func() {
		level.Info(logger).Log("transport", "http", "port", *healthPort, "msg", "healthcheck listening")
		http.ListenAndServe(fmt.Sprintf(":%d", *healthPort), health)
	}()

	mux := chi.NewRouter()
	mux.Mount("/services", svc.Routes())

	s := &http.Server{
		Addr:    fmt.Sprintf(":%d", *port),
		Handler: mux,
	}

	stopc := make(chan os.Signal, 1)
	signal.Notify(stopc, syscall.SIGINT, syscall.SIGTERM)

	httpLogger := log.With(logger, "transport", "http", "port", *port)
	go func(srv *http.Server) {
		level.Info(httpLogger).Log("msg", "listening")
		if err := s.ListenAndServe(); err != nil {
			level.Error(httpLogger).Log("msg", "error while listening and serving HTTP", "err", err)
			os.Exit(2)
		}
	}(s)

	// Graceful shutdown
	sig := <-stopc
	level.Info(httpLogger).Log("msg", "gracefully shutting down", "signal", sig)
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	s.Shutdown(ctx)
}
