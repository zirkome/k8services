package errors

// Error is an error intended for consumption by a REST API server; it can also be
// reconstructed by clients from a REST response.
type Error struct {
	KubernetesError error
}

func (e *Error) Error() string {
	return e.KubernetesError.Error()
}

func New(err error) *Error {
	return &Error{KubernetesError: err}
}
