FROM golang:1.10 as builder

WORKDIR /go/src/gitlab.com/zirkome/k8services
COPY . .

RUN go get -v github.com/Masterminds/glide && \
    glide install

RUN make static

FROM alpine:3.8

RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=builder /go/src/gitlab.com/zirkome/k8services/k8services /app

ENTRYPOINT ["./k8services"]
