package server

import (
	"encoding/json"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
	"github.com/pkg/errors"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes"

	serrors "gitlab.com/zirkome/k8services/pkg/errors"
)

// Server handles HTTP requests.
type Server struct {
	logger    log.Logger
	clientset kubernetes.Interface
}

// New instantiates a new Server with a custom logger and a Kubernetes Client.
func New(logger log.Logger, clientset *kubernetes.Clientset) *Server {
	return &Server{
		logger:    logger,
		clientset: clientset,
	}
}

// Routes returns a chi.Mux with the routes this server handles.
func (s *Server) Routes() *chi.Mux {
	router := chi.NewRouter()
	router.Get("/", s.ListServices)
	router.Get("/{serviceID}/pods", s.ListServicePods)
	router.Delete("/{serviceID}", s.DeleteService)
	return router
}

// ListServices returns a list of Kubernetes services inside the cluster.
func (s *Server) ListServices(w http.ResponseWriter, r *http.Request) {
	sl, err := s.clientset.CoreV1().Services(metav1.NamespaceDefault).List(metav1.ListOptions{})
	if err != nil {
		level.Error(s.logger).Log("err", err, "msg", "error listing services", "namespace", metav1.NamespaceDefault)
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusServiceUnavailable)
		json.NewEncoder(w).Encode(serrors.New(err))
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(sl)
}

// ListServicePods returns a list of pods that are behind the specified Kubernetes Service.
func (s *Server) ListServicePods(w http.ResponseWriter, r *http.Request) {
	serviceID := chi.URLParam(r, "serviceID")
	logger := log.With(s.logger, "namespace", metav1.NamespaceDefault, "service", serviceID)
	svc, err := s.clientset.CoreV1().Services(metav1.NamespaceDefault).Get(serviceID, metav1.GetOptions{})
	if err != nil {
		level.Error(logger).Log("err", err, "msg", "error get service")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusServiceUnavailable)
		json.NewEncoder(w).Encode(serrors.New(err))
		return
	}

	var labelSelector string
	for k, v := range svc.Spec.Selector {
		labelSelector += k + "=" + v + ","
	}
	labelSelector = strings.Trim(labelSelector, ",")

	pl, err := s.clientset.CoreV1().Pods(metav1.NamespaceDefault).List(metav1.ListOptions{
		LabelSelector: labelSelector,
	})
	if err != nil {
		level.Error(logger).Log("err", err, "msg", "error listing pods")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusServiceUnavailable)
		json.NewEncoder(w).Encode(serrors.New(err))
		return
	}
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	json.NewEncoder(w).Encode(pl)
}

// deleteOwners will delete the owners of a resource. If the a Owner is a ReplicaSet
// we check it has owners if so we go up the chain to find a deployment. If not we just
// delete the resource. Returns a Kubernetes Error if we couldn't delete the resource.
func (s *Server) deleteOwners(owners []metav1.OwnerReference) error {
	var deletionPropagation = metav1.DeletePropagationBackground
	for _, owner := range owners {
		switch owner.Kind {
		case "ReplicaSet":
			rs, err := s.clientset.
				Apps().
				ReplicaSets(metav1.NamespaceDefault).
				Get(owner.Name, metav1.GetOptions{})
			if err != nil {
				if apierrors.IsNotFound(err) {
					return nil
				}
				return err
			}
			if len(rs.GetOwnerReferences()) > 0 {
				if err := s.deleteOwners(rs.GetOwnerReferences()); err != nil {
					return errors.Wrap(err, "can't delete ReplicaSet's owners")
				}
				continue
			}
			if err := s.clientset.
				Apps().
				ReplicaSets(metav1.NamespaceDefault).
				Delete(owner.Name, &metav1.DeleteOptions{
					PropagationPolicy: &deletionPropagation,
				}); err != nil {
				if apierrors.IsNotFound(err) {
					return nil
				}
				return err
			}
			return nil
		case "Deployment":
			_, err := s.clientset.
				Apps().
				Deployments(metav1.NamespaceDefault).
				Get(owner.Name, metav1.GetOptions{})
			if err != nil {
				if apierrors.IsNotFound(err) {
					return nil
				}
				return err
			}
			if err := s.clientset.
				Apps().
				Deployments(metav1.NamespaceDefault).
				Delete(owner.Name, &metav1.DeleteOptions{
					PropagationPolicy: &deletionPropagation,
				}); err != nil {
				if apierrors.IsNotFound(err) {
					return nil
				}
				return err
			}
			return nil
		}
	}
	return nil

}

// DeleteService delete the specified Kubernetes service along with pods behind it and the resources
// managing theses Pods (i.e. Deployments and ReplicaSet).
func (s *Server) DeleteService(w http.ResponseWriter, r *http.Request) {
	serviceID := chi.URLParam(r, "serviceID")
	logger := log.With(s.logger, "namespace", metav1.NamespaceDefault, "service", serviceID)
	svc, err := s.clientset.CoreV1().Services(metav1.NamespaceDefault).Get(serviceID, metav1.GetOptions{})
	if err != nil {
		level.Error(logger).Log("err", err, "msg", "error get service")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusServiceUnavailable)
		json.NewEncoder(w).Encode(serrors.New(err))
		return
	}

	var labelSelector string
	for k, v := range svc.Spec.Selector {
		labelSelector += k + "=" + v + ","
	}
	labelSelector = strings.Trim(labelSelector, ",")

	pl, err := s.clientset.CoreV1().Pods(metav1.NamespaceDefault).List(metav1.ListOptions{
		LabelSelector: labelSelector,
	})
	if err != nil {
		level.Error(logger).Log("err", err, "msg", "error listing pods")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusServiceUnavailable)
		json.NewEncoder(w).Encode(serrors.New(err))
		return
	}
	for _, p := range pl.Items {
		if len(p.GetOwnerReferences()) > 0 {
			s.deleteOwners(p.GetOwnerReferences())
		} else {
			if err := s.clientset.Core().Pods(metav1.NamespaceDefault).Delete(p.Name, &metav1.DeleteOptions{}); err != nil {
				level.Error(logger).Log("err", err, "msg", "error deleting the pod", "pod", p.Name)
				w.Header().Add("Content-Type", "application/json")
				w.WriteHeader(http.StatusServiceUnavailable)
				json.NewEncoder(w).Encode(serrors.New(err))
				return
			}
		}
	}
	if err := s.clientset.Core().Services(metav1.NamespaceDefault).Delete(serviceID, &metav1.DeleteOptions{}); err != nil {
		level.Error(logger).Log("err", err, "msg", "error deleting service")
		w.Header().Add("Content-Type", "application/json")
		w.WriteHeader(http.StatusServiceUnavailable)
		json.NewEncoder(w).Encode(serrors.New(err))
		return
	}
	w.WriteHeader(http.StatusNoContent)
}
