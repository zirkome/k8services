package server

import (
	"context"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/go-chi/chi"
	"github.com/go-kit/kit/log"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/kubernetes/fake"
)

func TestServer_ListServices_NonEmpty(t *testing.T) {
	srv := &Server{
		clientset: fake.NewSimpleClientset(
			&corev1.Service{
				TypeMeta: metav1.TypeMeta{
					Kind: "Service",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "svc1",
					Namespace: metav1.NamespaceDefault,
				},
			},
			&corev1.Service{
				TypeMeta: metav1.TypeMeta{
					Kind: "Service",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "svc2",
					Namespace: metav1.NamespaceDefault,
				},
			}),
		logger: log.NewNopLogger(),
	}
	req, err := http.NewRequest(http.MethodGet, "/services", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(srv.ListServices)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"metadata":{},"items":[{"kind":"Service","metadata":{"name":"svc1","namespace":"default","creationTimestamp":null},"spec":{},"status":{"loadBalancer":{}}},{"kind":"Service","metadata":{"name":"svc2","namespace":"default","creationTimestamp":null},"spec":{},"status":{"loadBalancer":{}}}]}`
	if strings.TrimSpace(rr.Body.String()) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestServer_ListServices_Empty(t *testing.T) {
	srv := &Server{
		clientset: fake.NewSimpleClientset(),
		logger:    log.NewNopLogger(),
	}
	req, err := http.NewRequest(http.MethodGet, "/services", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(srv.ListServices)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"metadata":{},"items":null}`
	if strings.TrimSpace(rr.Body.String()) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestServer_ListServices_APINotWorking(t *testing.T) {
	// TODO(zirkome): Can't find a working way of making the fake client respond with a 5xx or 4xx.
	// Couldn't make fakeClientset.AddReactor and fakeClientset.AddProxyReactor work.
}

func TestServer_ListServicePods_NonEmpty(t *testing.T) {
	srv := &Server{
		clientset: fake.NewSimpleClientset(
			&corev1.Service{
				TypeMeta: metav1.TypeMeta{
					Kind: "Service",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app",
					Namespace: metav1.NamespaceDefault,
				},
				Spec: corev1.ServiceSpec{
					Selector: map[string]string{
						"app":  "the-app",
						"tier": "frontend",
					},
				},
			},
			&appsv1.Deployment{
				TypeMeta: metav1.TypeMeta{
					Kind: "Deployment",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app",
					Namespace: metav1.NamespaceDefault,
				},
			},
			&appsv1.ReplicaSet{
				TypeMeta: metav1.TypeMeta{
					Kind: "ReplicaSet",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app-rs1",
					Namespace: metav1.NamespaceDefault,
					OwnerReferences: []metav1.OwnerReference{
						metav1.OwnerReference{
							Kind:       "Deployment",
							APIVersion: "apps/v1",
							Name:       "the-app",
						},
					},
				},
			},
			&corev1.Pod{
				TypeMeta: metav1.TypeMeta{
					Kind: "Pod",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app-pod1",
					Namespace: metav1.NamespaceDefault,
					OwnerReferences: []metav1.OwnerReference{
						metav1.OwnerReference{
							Kind:       "ReplicaSet",
							APIVersion: "apps/v1",
							Name:       "the-app-rs1",
						},
					},
					Labels: map[string]string{
						"app":  "the-app",
						"tier": "frontend",
					},
				},
			},
			&corev1.Pod{
				TypeMeta: metav1.TypeMeta{
					Kind: "Pod",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app-pod2",
					Namespace: metav1.NamespaceDefault,
					OwnerReferences: []metav1.OwnerReference{
						metav1.OwnerReference{
							Kind:       "ReplicaSet",
							APIVersion: "apps/v1",
							Name:       "the-app-rs1",
						},
					},
					Labels: map[string]string{
						"app":  "the-app",
						"tier": "frontend",
					},
				},
			}),
		logger: log.NewNopLogger(),
	}
	req, err := http.NewRequest(http.MethodGet, "/services/the-app/pods", nil)
	if err != nil {
		t.Fatal(err)
	}
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("serviceID", "the-app")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(srv.ListServicePods)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"metadata":{},"items":[{"kind":"Pod","metadata":{"name":"the-app-pod1","namespace":"default","creationTimestamp":null,"labels":{"app":"the-app","tier":"frontend"},"ownerReferences":[{"apiVersion":"apps/v1","kind":"ReplicaSet","name":"the-app-rs1","uid":""}]},"spec":{"containers":null},"status":{}},{"kind":"Pod","metadata":{"name":"the-app-pod2","namespace":"default","creationTimestamp":null,"labels":{"app":"the-app","tier":"frontend"},"ownerReferences":[{"apiVersion":"apps/v1","kind":"ReplicaSet","name":"the-app-rs1","uid":""}]},"spec":{"containers":null},"status":{}}]}`
	if strings.TrimSpace(rr.Body.String()) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestServer_ListServicePods_NoPods(t *testing.T) {
	srv := &Server{
		clientset: fake.NewSimpleClientset(
			&corev1.Service{
				TypeMeta: metav1.TypeMeta{
					Kind: "Service",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app",
					Namespace: metav1.NamespaceDefault,
				},
				Spec: corev1.ServiceSpec{
					Selector: map[string]string{
						"app":  "the-app",
						"tier": "frontend",
					},
				},
			},
		),
		logger: log.NewNopLogger(),
	}
	req, err := http.NewRequest(http.MethodGet, "/services/the-app/pods", nil)
	if err != nil {
		t.Fatal(err)
	}
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("serviceID", "the-app")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(srv.ListServicePods)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"metadata":{},"items":null}`
	if strings.TrimSpace(rr.Body.String()) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestServer_ListServicePods_ServiceNotFound(t *testing.T) {
	srv := &Server{
		clientset: fake.NewSimpleClientset(),
		logger:    log.NewNopLogger(),
	}
	req, err := http.NewRequest(http.MethodGet, "/services/the-app/pods", nil)
	if err != nil {
		t.Fatal(err)
	}
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("serviceID", "the-app")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(srv.ListServicePods)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusServiceUnavailable {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := `{"KubernetesError":{"ErrStatus":{"metadata":{},"status":"Failure","message":"services \"the-app\" not found","reason":"NotFound","details":{"name":"the-app","kind":"services"},"code":404}}}`
	if strings.TrimSpace(rr.Body.String()) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestServer_DeleteService(t *testing.T) {
	srv := &Server{
		clientset: fake.NewSimpleClientset(
			&corev1.Service{
				TypeMeta: metav1.TypeMeta{
					Kind: "Service",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app",
					Namespace: metav1.NamespaceDefault,
				},
				Spec: corev1.ServiceSpec{
					Selector: map[string]string{
						"app":  "the-app",
						"tier": "frontend",
					},
				},
			},
			&appsv1.Deployment{
				TypeMeta: metav1.TypeMeta{
					Kind: "Deployment",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app",
					Namespace: metav1.NamespaceDefault,
				},
			},
			&appsv1.ReplicaSet{
				TypeMeta: metav1.TypeMeta{
					Kind: "ReplicaSet",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app-rs1",
					Namespace: metav1.NamespaceDefault,
					OwnerReferences: []metav1.OwnerReference{
						metav1.OwnerReference{
							Kind:       "Deployment",
							APIVersion: "apps/v1",
							Name:       "the-app",
						},
					},
				},
			},
			&corev1.Pod{
				TypeMeta: metav1.TypeMeta{
					Kind: "Pod",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app-pod1",
					Namespace: metav1.NamespaceDefault,
					OwnerReferences: []metav1.OwnerReference{
						metav1.OwnerReference{
							Kind:       "ReplicaSet",
							APIVersion: "apps/v1",
							Name:       "the-app-rs1",
						},
					},
					Labels: map[string]string{
						"app":  "the-app",
						"tier": "frontend",
					},
				},
			},
			&corev1.Pod{
				TypeMeta: metav1.TypeMeta{
					Kind: "Pod",
				},
				ObjectMeta: metav1.ObjectMeta{
					Name:      "the-app-pod2",
					Namespace: metav1.NamespaceDefault,
					OwnerReferences: []metav1.OwnerReference{
						metav1.OwnerReference{
							Kind:       "ReplicaSet",
							APIVersion: "apps/v1",
							Name:       "the-app-rs1",
						},
					},
					Labels: map[string]string{
						"app":  "the-app",
						"tier": "frontend",
					},
				},
			},
		),
		logger: log.NewNopLogger(),
	}
	req, err := http.NewRequest(http.MethodDelete, "/services/the-app", nil)
	if err != nil {
		t.Fatal(err)
	}
	rctx := chi.NewRouteContext()
	rctx.URLParams.Add("serviceID", "the-app")
	req = req.WithContext(context.WithValue(req.Context(), chi.RouteCtxKey, rctx))

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(srv.DeleteService)

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	if status := rr.Code; status != http.StatusNoContent {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := ``
	if strings.TrimSpace(rr.Body.String()) != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
